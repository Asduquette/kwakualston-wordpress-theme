<?php
// Setup  -- Probably want to keep this stuff... 


require_once( dirname(__FILE__) . '/setup.php' );

/* ! Kwakushop Alternate Logo */

 /*
 * The main site logo template
 *
 * @package     PageLines Framework
 * @subpackage  Functions Library
 *
 * @since       1.1.0
 *
 * @param       null $location
 *
 * @uses        (global) $pagelines_ID
 * @uses        ploption - pagelines_custom_logo, pagelines_custom_logo_url
 * @uses        (filters) pagelines_logo_url, pagelines_site_logo, pagelines_custom_logo_url, pagelines_site_title
 *
 */
 


/* ! Jigoshop template overrides */ 
    /* ! source file: plugins/jigoshop/jigoshop_template_functions.php line 250  */ 



/*
    single product:
    image-left          title (black)
                        category / price (black)
                        -
                        Specifications (all black)
                        line
                        line
                        line
                        -
                        
                        description (no title) (gray #999)  (using excerpt for now)
                        
                        - (orange)
                        price (orange)
                        subtract quanty icon
                        quantity    icon
                        add quantity Icon
                        
                        gray add to cart button */
 



/**
 * Products Loop : override jigoshop_template_functions.php - beginning line : 88
 **/
if (!function_exists('jigoshop_template_loop_add_to_cart')) {
	function jigoshop_template_loop_add_to_cart( $post, $_product ) {

		do_action('jigoshop_before_add_to_cart_button');

		// do not show "add to cart" button if product's price isn't announced
		if ( $_product->get_price() === '' AND ! ($_product->is_type(array('variable', 'grouped', 'external'))) ) return;

		if ( $_product->is_in_stock() OR $_product->is_type('external') ) :
			if ( $_product->is_type(array('variable', 'grouped')) ) :
				$output = '<a href="'.get_permalink($_product->id).'" class="button">'.__('Select', 'jigoshop').'</a>';
			elseif ( $_product->is_type('external') ) :
				$output = '<a href="'.get_post_meta( $_product->id, 'external_url', true ).'" class="button">'.__('Buy product', 'jigoshop').'</a>';
			else :
				$output = '<a href="'.esc_url($_product->add_to_cart_url()).'" class="button" rel="nofollow">'.__('Add to cart', 'jigoshop').'</a></span>';
			endif;
		elseif ( ($_product->is_type(array('grouped')) ) ) :
			return;
		else :
			$output = '<span class="nostock">'.__('Out of Stock', 'jigoshop').'</span>';
		endif;
		
		echo apply_filters( 'jigoshop_loop_add_to_cart_output', $output, $post, $_product );

		do_action('jigoshop_after_add_to_cart_button');

	}
}

if (!function_exists('jigoshop_template_loop_price')) {
	function jigoshop_template_loop_price( $post, $_product ) {
		    echo $_product->get_categories( ', ', ' <p class="category-price-section"><span class="posted_in">' . __( '', 'jigoshop' ) . '', ' / </span>'); ?><span class="price"><?php echo apply_filters( 'jigoshop_single_product_price', $_product->get_price_html() );
		    echo '</span></p>';
		    
		
		
	}
}

/**
 * Product summary box on single product pages : override jigoshop_template_functions.php - beginning line : 230ish
 **/
if (!function_exists('jigoshop_template_single_title')) {
	function jigoshop_template_single_title( $post, $_product ) {
		?><h1 class="product_title page-title"><?php echo apply_filters( 'jigoshop_single_product_title', the_title( '', '', false ) ); ?></h1><?php
	}
}

if (!function_exists('jigoshop_template_single_price')) {
	function jigoshop_template_single_price( $post, $_product ) {
		    echo $_product->get_categories( ', ', ' <p class="category-price-section"><span class="posted_in">' . __( '', 'jigoshop' ) . '', ' / </span>'); ?><span class="price">
		    <?php echo apply_filters( 'jigoshop_single_product_price', $_product->get_price_html() );
		    echo '<br>-<br></p>';
		    //get specifications from attributes section
		    //global $_product;
		//echo '<div>';
		//echo '<h2>' . apply_filters('jigoshop_product_attributes_heading', __('', 'jigoshop')) . '</h2>';
		//echo apply_filters( 'jigoshop_single_product_attributes', $_product->list_attributes() );
		//echo '</div>';
		 echo the_content();
	}
}

if (!function_exists('jigoshop_template_single_excerpt')) {
	function jigoshop_template_single_excerpt( $post, $_product ) {
		if ($post->post_excerpt) //echo apply_filters( 'jigoshop_single_product_excerpt', wpautop(wptexturize($post->post_excerpt)) );
		echo '<p class="divider">-</p><p class="single-product-price">';
		echo apply_filters( 'jigoshop_single_product_price', $_product->get_price_html() );
		echo '</p>';
	}
}



if (!function_exists('jigoshop_template_single_meta')) {
	function jigoshop_template_single_meta( $post, $_product ) {

        $jigoshop_options = Jigoshop_Base::get_options();
		echo '<div class="product_meta">';
		if ($jigoshop_options->get_option('jigoshop_enable_sku')=='yes' && !empty($_product->sku)) :
			echo '<div class="sku">'.__('SKU','jigoshop').': ' . $_product->sku . '</div>';
		endif;

		echo $_product->get_categories( ', ', ' <div class="posted_in">' . __( 'Categories: ', 'jigoshop' ) . '', '.</div>');
		echo $_product->get_tags( ', ', ' <div class="tagged_as">' . __( 'Tags: ', 'jigoshop' ) . '', '.</div>');
		echo '</div>';
		

	}
} 

if (!function_exists('jigoshop_simple_add_to_cart')) {
	function jigoshop_simple_add_to_cart() { ?><?php
	 global $_product; $availability = $_product->get_availability(); ?>
	 
		
		
		<form action="<?php echo esc_url( $_product->add_to_cart_url() ); ?>" class="cart" method="post">
			<?php do_action('jigoshop_before_add_to_cart_form_button'); ?>
		 	<div class="quantity"><input name="quantity" value="1" size="4" title="Qty" class="input-text qty text" maxlength="12" /></div>
		 	<button type="submit" class="button-alt"><?php _e('Add to cart', 'jigoshop'); ?></button>
		 	<?php do_action('jigoshop_add_to_cart_form'); ?>
		</form>
		<?php
	}
}



