<style type="text/css">
.pl-dash.welcome p {color:#888;text-shadow:0 1px 0 rgba(255,255,255,0.7);}
.pl-dash.welcome {margin-top:25px;}
.pl-dash.welcome h3 {font-size: .99em; color: #555;}
</style>
<div class="theme_intro_pad">
	<div class="admin_billboard fix">
		<div class="admin_billboard_pad fix">
			<div class="admin_theme_screenshot"><img class="" src="<?php bloginfo('stylesheet_directory'); ?>/thumb.png"></div>
				<div class="admin_billboard_content">
					<div class="admin_header"><h3 class="admin_header_main">hi</h3></div>
					<div class="admin_billboard_text">Welcome to your new custom theme</div>
					<div class="clear"></div>
				</div>
		    </div>
	</div>

		<div class="pl-dash welcome">
			<div class="pl-dash-pad">
				<div class="pl-vignette">
					<h2 class="dash-title">Adding &amp; Modifying Pages</h2>
					<div class="pl-dashboard-story media  dashpane">
						<div class="dashpane-pad fix">
							<div class="bd">
								<h3>Page Templates : 3 Boxes</h3>
								<p>This template is used for the home and motion pages</p>
								<h3>Page Templates : Template 1</h3>
								<p>This template is used for the home and motion pages</p>
								<h3>Page Templates : Template 2</h3>
								<p>This template is used for the home and motion pages</p>
								<h3>Page Templates : Template 3</h3>
								<p>This template is used for the gallery pages</p>
								<h3>Page Templates : Templates 4 and 5</h3>
								<p>These templates can be customized</p>
								<h3>Page Templates : Kwakushop</h3>
								<p>This template is used for the shop checkout pages</p>
								
								
														
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="pl-dash welcome">
			<div class="pl-dash-pad">
				<div class="pl-vignette">
				<h2 class="dash-title">Sections Explained</h2>
					<div class="pl-dashboard-story media dashpane">
						<div class="dashpane-pad fix">
							<div class="bd">
    							<h3>Header Sections</h3>
    							<ul><li><strong>Kwakushop Header</strong> is the KShop logo</li>
								    <li><strong>Branding</strong> is the KwakuAlston logo</li>
								    <li><strong>Primary Nav</strong> is the KwakuAlston portfolio navigation menu with the dotted top and bottom borders</li>
								    <li><strong>Nav Classic</strong> is menu that contains teh social icons to the right</li>
								    <li><strong>NavBar</strong> contains the K'Shop navigation menu with the dashed top and bottom borders</li>
    							</ul>
    							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="pl-dash welcome">
			<div class="pl-dash-pad">
				<div class="pl-vignette">
					<h2 class="dash-title">Adding &amp; Editing Galleries</h2>
					<div class="pl-dashboard-story media  dashpane">
						<div class="dashpane-pad fix">
							<div class="bd">
								<p>How to add a new gallery</p>						
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="pl-dash welcome">
			<div class="pl-dash-pad">
				<div class="pl-vignette">
					<h2 class="dash-title">Add new videos or gallery links to the Home and/or Motion pages</h2>
					<div class="pl-dashboard-story media  dashpane">
						<div class="dashpane-pad fix">
							<div class="bd">
								<p>How to add a new gallery</p>						
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="pl-dash welcome">
			<div class="pl-dash-pad">
				<div class="pl-vignette">
					<h2 class="dash-title">Adding Jigoshop Products</h2>
					<div class="pl-dashboard-story media  dashpane">
						<div class="dashpane-pad fix">
							<div class="bd">
								<p>The meta window is at the bottom of the edit pages<img src="http://kwakualston.com/wp-server/wp-content/uploads/2012/12/instruction-edit-meta.png"></p>
							</div>
						</div>
					</div>
					<h2 class="dash-title">What if you want a New Product Category added to the secondary nav menu?</h2>
					<div class="pl-dashboard-story media  dashpane">
						<div class="dashpane-pad fix">
							<div class="bd">
								<p></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		

</div>