var gulp = require('gulp');
var plumber = require('gulp-plumber');
var clean = require('gulp-clean');
var rename = require('gulp-rename');
var flatten = require('gulp-flatten');
var tap = require('gulp-tap');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var minify = require('gulp-minify-css');
var browserSync = require('browser-sync');


var paths = {
  output : 'dist/',
  styles : {
    input : 'scss/**/*.scss',
    output : 'dist/css/'
  }
};
gulp.task('browser-sync', function() {
  var files = [
    'dist/css/main.css',
    'dist/css/mainmin.css',
    '*.php',
    '*.html',
    '! functions.php'
  ];

  browserSync.init(files, {
    proxy: "http://kwakualston-orig.dev:8888/"
  });
});


gulp.task('styles', function() {
  return gulp.src(paths.styles.input)
    .pipe(plumber())
    .pipe(sass( {
      outputStyle: 'expanded'
    }))
    .pipe(flatten())
    .pipe(prefix('last 2 version', '> 1%'))
    .pipe(gulp.dest(paths.styles.output))
    .pipe(rename({ suffix: 'min'}))
    .pipe(minify())
    .pipe(gulp.dest(paths.styles.output))
    .pipe(browserSync.reload({stream:true}));
});


gulp.task('watch', function(){
      gulp.watch(paths.styles.input, ['styles']);
      gulp.watch('*.php', ['browser-sync']);
      gulp.watch('*.css', ['browser-sync']);

});
// Default task to be run with `gulp`
gulp.task('default', ['styles', 'watch', 'browser-sync'] );

// + Date.now() 