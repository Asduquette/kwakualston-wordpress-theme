/**
*	@name							Custom Scripts
*	@descripton				        KwakuAlston
*	@version						2.0
*	@requires						Jquery 1.2.6+
*
*/

jQuery(document).ready(function() {
   
   jQuery('div.ngg-imagebrowser-desc').hide();
   
   jQuery('.view-caption-button').click(function(){
     jQuery('div.ngg-imagebrowser-desc').toggle();
   });
 });
 
 /* neutral show/hide */
 
 jQuery(document).ready(function() {
   
   //jQuery('div.toggle-container').hide();
   
   jQuery('.view-container-button').click(function(){
     jQuery('div.toggle-container').toggle();
   });
 });