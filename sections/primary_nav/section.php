<?php
/*
Section: Primary Nav
Author: duquette design
Author URI: http://andreaduquette.net
Version: 1.2.0
Description: Custom Navigation Menu for kwakualston.com
Class Name: PrimaryNav
Cloning: true
*/

/*
 * PageLines Headers API
 * 
 *  Sections support standard WP file headers (http://codex.wordpress.org/File_Header) with these additions:
 *  -----------------------------------
 * 	 - Section: The name of your section.
 *	 - Class Name: Name of the section class goes here, has to match the class extending PageLinesSection.
 *	 - Cloning: (bool) Enable cloning features.
 *	 - Depends: If your section needs another section loaded first set its classname here.
 *	 - Workswith: Comma seperated list of template areas the section is allowed in.
 *	 - Failswith: Comma seperated list of template areas the section is NOT allowed in.
 *	 - Demo: Use this to point to a demo for this product.
 *	 - External: Use this to point to an external overview of the product
 *	 - Long: Add a full description, used on the actual store page on http://www.pagelines.com/store/
 *
 */

/**
 *
 * Section Class Setup
 * 
 * Name your section class whatever you want, just make sure it matches the 
 * "Class Name" in the section meta (above)
 * 
 * File Naming Conventions
 * -------------------------------------
 *  section.php 		- The primary php loader for the section.
 *  style.css 			- Basic CSS styles contains all structural information, no color (autoloaded)
 *  images/				- Image folder. 
 *  thumb.png			- Primary branding graphic (300px by 225px - autoloaded)
 *  screenshot.png		- Primary Screenshot (300px by 225px)
 *  screenshot-1.png 	- Additional screenshots: (screenshot-1.png -2 -3 etc...optional).
 *  icon.png			- Portable icon format (16px by 16px)
 *	color.less			- Computed color control file (autoloaded)
 *
 */
        class PrimaryNav extends PageLinesSection {
        
        	function section_persistent(){
        		register_nav_menus( array( 'primary_nav' => __( 'Primary Nav Section', 'pagelines' ) ) );
        	
        		add_filter('pagelines_css_group', array(&$this, 'section_selectors'), 10, 2);
        
        	}
        
        	function section_selectors($selectors, $group){
        
        		$s['nav_standard'] = '.main-nav li:hover, .main-nav .current-page-ancestor a,  .main-nav li.current-page-ancestor ul a, .main-nav li.current_page_item a, .main-nav li.current-menu-item a, .sf-menu li li, .sf-menu li li li';
        
        		$s['nav_standard_border'] = 'ul.sf-menu ul li';
        
        		$s['nav_highlight'] = '.main-nav li a:hover, .main-nav .current-page-ancestor .current_page_item a, .main-nav li.current-page-ancestor ul a:hover';
        
        
        		if($group == 'box_color_primary')
        			$selectors .= ','.$s['nav_standard'];
        		elseif($group == 'box_color_secondary')	
        			$selectors .= ','.$s['nav_highlight'];
        		elseif( $group == 'border_primary' )
        			$selectors .= ','.$s['nav_standard_border'];
        
        		return $selectors;
        	}
        	
        	
           function section_template() { 
        
        	if(function_exists('wp_nav_menu'))
        		wp_nav_menu( array('menu_class'  => 'inline-list main_nav font-sub', 'theme_location'=>'primary_nav','depth' => 1,  'fallback_cb'=>'primary_nav_fallback') );
        	else
        		nav_fallback();
        	}
        
        }
        
        if(!function_exists('primary_nav_fallback')){
        	function primary_nav_fallback() {
        		printf('<ul id="primary_nav_fallback" class="main_nav inline-list primarynav font-sub">%s</ul>', wp_list_pages( 'title_li=&sort_column=menu_order&depth=1&echo=0') );
        	}
        }