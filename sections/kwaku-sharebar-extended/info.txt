== Description ==

ShareBar Extended is a post and page social sharing tool that seamlessly matches the PageLines layout. Sharing options include Google+, Pinterest, Facebook, Twitter, StumbleUpon, Reddit, Del.icio.us, Digg, LinkedIn, and Email. Works in post excerpts too!

<h3>New in version 1.7!</h3>
Version 1.7 was created to ensure compatibility with PageLines Framework v2.3. You won't notice too much as most changes were code-related, but I did add a feature which gives social sharing icons a hover effect. :)

If you have any requests or questions, you can leave a comment below or on <a href="http://www.simplemama.com/sharebar-extended/" target="_blank" title="Simple Mama">my website</a>. Don't forget to give this product a 5-star rating if you love it!

I keep all my add-ons inexpensive (or free!) to give you a better PageLines experience. If you appreciate this or just want to say thanks, you can <a href="http://www.simplemama.com/" target="_blank" title="Simple Mama">buy me a cup of coffee</a>.

&hearts; Jenny, your Simple Mama

== Changelog ==

= 1.7 =
* Change - Code changed for compatibility with PageLines Framework v2.3.
* Change - Code cleanup for correct ampersand usage in URLs.
* Added - Hover effect on social icons.
* Added - This document.
* Added - Alt value to Share by Email image.

= 1.63 =
* Added - Allowed use in all PageLines content sections.

= 1.62 =
* Fixed - Bug fix.

= 1.6 =
* Added - Added the ability to use in post excerpts.

= 1.52 = 
* Fixed - Fixed another coding error related to Pinterest image sharing.

= 1.51 =
* Fixed - Fixed a coding error that I forgot to remove before releasing new version. Oops.

= 1.5 =
* Change - Major overhaul of the section.
* Change - Internal coding reworked to avoid conflicts with the default PageLines sharebar. You will need to visit the new admin panel to re-save your options.
* Added - New admin tab located at PageLines -> Settings -> ShareBar Extended in order to avoid conflicts with the default PageLines sharebar.
* Added - Enabled cloning.
* Added - All new sharing icons.
* Added - Share by email option.
* Added - Font family (uses GoogleFonts), font size, added font color for sharing text. Also added "float" options to provide positioning options.

= 1.31 =
* Fixed - Pinterest URL fix.

= 1.3 =
* Fixed - Corrected Pinterest URL to properly pass the post title and post URL to the Pinterest description field.

= 1.2 =
* Fixed - Code cleanup thanks to Simon @ PageLines.

= 1.1 =
* Added - PageLines Framework 2.1 update removed the original share bar. This release adds it back in and is updated to work with 2.1.
* Added - Post Footer Social Sharing Text is now part of this section since it was also removed as part of the framework update.
* Added - Stylesheet added since the framework update removed padding between icons.

= 1.0 =
* Initial release.