<?php
/*
	Section: kwaku ShareBar Extended
	Author: Simple Mama Blog Design and demo
	Author URI: http://www.simplemama.com/
	External: http://www.simplemama.com/sharebar-extended/
	Demo: http://www.simplemama.com/sharebar-extended/
	Description: ShareBar Extended is a post and page social sharing tool that seamlessly matches the PageLines layout. Sharing options include Google+, Pinterest, Facebook, Twitter, StumbleUpon, Reddit, Del.icio.us, Digg, LinkedIn, and Email. Works in post excerpts too!
	Version: 1.7
	Class Name: PLShareBarExtended
	Workswith: main
	Cloning: true
*/
class PLShareBarExtended extends PageLinesSection {
/***** MAKE THE OPTIONS DISPLAY ON THE ADMIN PANEL *****/
	function section_persistent() {
		if (is_admin()) {
			add_filter('pagelines_options_array', array(&$this, 'sharebar_extended_options'));
		}
		if (!is_admin()) {
			add_filter('pl-dynamic-css', array(&$this, 'draw_css'));
		}
		if (ploption('sbe_share_under_excerpt')) {
			add_filter('pagelines_post_header', array(&$this,'sbe_show_under_excerpt'), 10, 2);
		}
	}
	function section_head($clone_id) {
		$prefix = ($clone_id != '') ? '.clone_'.$clone_id : '';
		echo load_custom_font(ploption('post_footer_social_font', $this->oset), $prefix.'.sharebar_text');
	}
/***** WRITE THE CSS FOR THE FONT SIZE *****/
	function draw_css($css) {
		$textSize = sprintf('%spx', ploption('new-font-size'));
		$textColor = sprintf('%s', ploption('new-font-color'));
		$add = sprintf('.sharebar_text {font-size: %s;}', $textSize);
		$add .= sprintf('.sharebar_text {color: %s;}', $textColor);
		if (ploption('sbe_share_under_excerpt')) {
			$add .= sprintf('.sbe-share .sharebar_text {display: none;}
							.sbe-share .sbe_sharelink {padding: 0 0 0 5px; vertical-align: top;//for 2.3+}
							.sbe-share .sbe_pin_shareimg {margin-bottom: -1px; vertical-align: top;//for 2.3+}
							.sbe-share .sbe_shareimg {vertical-align: top;//for 2.3+}
							.sbe-share .sbe_shareimg:hover, .sbe-share .sbe_pin_shareimg:hover {opacity: 0.6;}');
		}
		return $css . $add;
	}
/***** FUCTIONS FOR UNDER EXCERPT AND UNDER META SHARING *****/
	function sbe_show_under_excerpt($input, $format){
		if (!class_exists('PLShareBarExtended') || $format == 'clip' || is_single() || is_page())
			return $input;
		global $post;
		ob_start();
		$this->section_template();
		$shareit = ob_get_clean();
		$sbe_share = sprintf('<div class="sbe-share">%s</div>', $shareit);
		return $input.$sbe_share;
	}
/***** RENDER THE OPTIONS *****/
	function section_template() {
	$sharepre  = '';
	$sharepost = '';
	global $post; ?>
	<div class="post-footer">
		<div class="post-footer-pad">
			<div class="sharebar_text <?php
				if (ploption('text_float') == 'left')
					echo 'left';
				if (ploption('text_float') == 'right')
					echo 'right';
			?>"><?php echo ploption('post_footer_social_text');?></div>
			<div class="sharebar_icons <?php
				if (ploption('icon_float') == 'left')
					echo 'left';
				if (ploption('icon_float') == 'right')
					echo 'right';
			?>">
				<?php
				$upermalink = urlencode( get_permalink( $post->ID ) );
				$utitle = urlencode( strip_tags( get_the_title() ) );
				$string = '<a class="sbe_sharelink" href="%s" title="%s" rel="nofollow" target="_blank"><img class="sbe_shareimg" src="%s" alt="%s" /></a>';
				$pinstring = '<a class="sbe_sharelink" href="%s" title="%s" rel="nofollow" target="_blank"><img class="sbe_pin_shareimg" src="%s" alt="%s" /></a>';
				echo apply_filters( 'pagelines_before_sharebar', $sharepre );// Hook
				
				/* ! pinterest */
				/***** NEEDED FOR PINTEREST *****/
				$size = 'large';
				$image_id = get_post_thumbnail_id();
				$image_src = wp_get_attachment_image_src($image_id,$size);
				$image_url = $image_src[0];
				/***** END PINTEREST *****/
				
				// ! googleplus
				if(ploption('sbe_share_googleplus')){
					printf('<div class="g-plusone" data-size="medium" data-annotation="none"></div>');
				}
				// ! pinterest 2
				if(ploption('sbe_share_pinterest')){
					$url = sprintf('http://pinterest.com/pin/create/button/?url=%s&amp;media=%s&amp;description=%s on %s', $upermalink, $image_url, $post->post_title, $upermalink);
					printf($pinstring, $url, 'Share on Pinterest', $this->base_url.'/sbe_pinit.png', 'Pinterest');
				}
				// ! facebook
				if(ploption('sbe_share_facebook')){
					$url = sprintf('http://www.facebook.com/sharer.php?u=%s&amp;t=%s', $upermalink, $utitle);
					printf($string, $url, 'Share on Facebook', $this->base_url.'/facebook.png', 'Facebook');
				}
				// ! twitter
				if(ploption('sbe_share_twitter')){
					$url = sprintf('http://twitter.com/home?status=%s%s%s', pagelines_format_tweet(urlencode(html_entity_decode(get_the_title())), get_permalink($post->ID)), (ploption('twittername')) ? (' @'.ploption('twittername')) : '', (' '.urlencode(html_entity_decode(ploption('site-hashtag')))));
					printf($string, $url, 'Share on Twitter', $this->base_url.'/twitter.png', 'Twitter');
				}
				// ! stumbleupon
				if(ploption('sbe_share_stumbleupon')){
					$url = sprintf('http://www.stumbleupon.com/submit?url=%s&amp;title=%s', $upermalink, $utitle);
					printf($string, $url, 'Share on StumbleUpon', $this->base_url.'/stumble.png', 'StumbleUpon');
				}
				// ! reddit
				if(ploption('sbe_share_reddit')){
					$url = sprintf('http://reddit.com/submit?phase=2&amp;url=%s&amp;title=%s', $upermalink, $utitle);
					printf($string, $url, 'Share on Reddit', $this->base_url.'/reddit.png', 'Reddit');
				}
				// ! delicious
				if(ploption('sbe_share_delicious')){
					$url = sprintf('http://del.icio.us/post?url=%s&amp;title=%s', $upermalink, $utitle);
					printf($string, $url, 'Share on Delicious', $this->base_url.'/delicious.png', 'Delicious');
				}
				// ! diggit
				if(ploption('sbe_share_digg')){
					$url = sprintf('http://digg.com/submit?phase=2&amp;url=%s&amp;title=%s', $upermalink, $utitle);
					printf($string, $url, 'Share on Digg', $this->base_url.'/digg.png', 'Digg');
				}
				// ! linkedin
				if(ploption('sbe_share_linkedin')){
					$url = sprintf('http://www.linkedin.com/shareArticle?&amp;url=%s&amp;title=%s', $upermalink, $utitle);
					printf($string, $url, 'Share on LinkedIn', $this->base_url.'/linkedin.png', 'LinkedIn');
				}
				// ! email
				if(ploption('sbe_share_email')){
					?><a class="sbe_sharelink" href="mailto:" title="Share by Email"><img class="sbe_shareimg" src="<?php echo ($this->base_url.'/email.png') ?>" alt="Share by Email" /></a><?php
					/*printf($string, $url, 'Share by Email', $this->base_url.'/email.png', 'Email');*/
				}
				/*if(ploption('sbe_share_print')){
					?><a class="sbe_sharelink" href="javascript:window.print()" title="Print This Page"><img src="<?php echo ($this->base_url.'/print.png') ?>" alt="" /></a><?php
					//printf($string, $url, 'Print This Page', $this->base_url.'/print.png', 'Print');
				}*/
				echo apply_filters( 'pagelines_after_sharebar', $sharepost ); // Hook
				?>
			</div>
		<div class="clear"></div>
		</div>
	</div>
	<?php
	}
/***** THE ADMIN OPTIONS *****/
	function sharebar_extended_options($original_option_array){
		$post_footer_share_links_new['ShareBar_Extended'] = array(
			'post_footer_share_links' => array(
				'default'	=> '',
				'type'		=> 'check_multi',
				'selectvalues'	=> array(
					'sbe_share_googleplus'	=> array('inputlabel'=> 'Google+ Sharing Icon', 'default'=> false),
					'sbe_share_pinterest'	=> array('inputlabel'=> 'Pinterest Sharing Icon', 'default'=> false),
					/*'sbe_like_facebook'		=> array('inputlabel'=> 'Facebook Like Icon', 'default'=> true),*/
					'sbe_share_facebook'	=> array('inputlabel'=> 'Facebook Sharing Icon', 'default'=> true),
					'sbe_share_twitter'		=> array('inputlabel'=> 'Twitter Sharing Icon', 'default'=> true),
					'sbe_share_stumbleupon'	=> array('inputlabel'=> 'StumbleUpon Sharing Icon', 'default'=> false),
					'sbe_share_reddit'		=> array('inputlabel'=> 'Reddit Sharing Icon', 'default'=> false),
					'sbe_share_delicious'	=> array('inputlabel'=> 'Del.icio.us Sharing Icon', 'default'=> false),
					'sbe_share_digg'		=> array('inputlabel'=> 'Digg Sharing Icon', 'default'=> false),
					'sbe_share_linkedin'	=> array('inputlabel'=> 'LinkedIn Sharing Icon', 'default'=> false),
					'sbe_share_email'		=> array('inputlabel'=> 'Email Sharing Icon', 'default'=> false),
					/*'sbe_share_print'		=> array('inputlabel'=> 'Print Icon', 'default'=> false)*/
					'sbe_share_under_excerpt'	=> array('inputlabel'=> 'Add Social Icons Under Excerpts (Blog Page)', 'default'=> false)
				),
				'inputlabel'	=> 'Select Which Sharing Icons to Use',
				'title'		=> 'ShareBar Extended: Social Sharing Icons',
				'shortexp'	=> 'Where would you like your readers to share your content?',
				'exp'		=> 'Select which social sharing icons will show in your posts and pages.'
			),
			'text_float' => array(
				'default'	=> 'left',
				'type'		=> 'select',
				'inputlabel'	=> 'Text Alignment',
				'exp'		=> 'Do you want the Social Sharing Text on the left or right?',
				'selectvalues' => array(
					'left' => array('name' => 'Text on Left'),
					'right' => array('name' => 'Text on Right'),
				),
			),
			'icon_float' => array(
				'default'	=> 'right',
				'type'		=> 'select',
				'inputlabel'	=> 'Icons Alignment',
				'exp'		=> 'Do you want the Social Sharing Icons on the left or right? This applies to "Social Icons Under Excerpts" also!',
				'selectvalues' => array(
					'left' => array('name' => 'Icons on Left'),
					'right' => array('name' => 'Icons on Right'),
				),
			),
			'post_footer_social_text' => array(
				'default'	=> 'If you like this, please share it!',
				'type'		=> 'text',
				'inputlabel'	=> 'Social Sharing Text',
				'exp'		=> 'Set the text to display next to your social sharing icons. Example: "If you like this, please share it!"'
			),
			'post_footer_social_font'	=> array(
				'type'		=> 'fonts',
				'inputlabel'	=> 'Choose a Font',
				'exp'		=> 'Select which font you would like to use for the Social Sharing Text.'
			),
			'post_footer_social_font_size'	=> array(
				'type'		=> 'text_multi',
				'inputsize'	=> 'tiny',
				'exp'		=> 'Select the font size for the Social Sharing Text. Enter a numerical value only (without the px).',
				'selectvalues'	=> array(
					'new-font-size'	=> array(
						'default'	=> '14',
						'inputlabel'	=> 'Enter a Font Size (in px) for the Social Sharing Text',
						//'css_prop'	=> 'font-size',
						//'selectors'	=> '#sharebar-extended .sharebar_text',
					),
				),
			),
			'post_footer_social_font_color'	=> array(
				'type'		=> 'color_multi',
				'selectvalues'	=> array(
					'new-font-color'=> array(
						'default'	=> '',
						'inputlabel'=> 'Select the Font Color for<br />the Social Sharing Text',
						//'css_prop'	=> 'color',
						//'selectors'	=> '.sharebar_text',
					),
				),
			),
		);
		return array_merge($original_option_array, $post_footer_share_links_new);
	}
}//end class
new PLShareBarExtended;
/***** ADD SCRIPTS TO WP_HEAD *****/
function sbe_scripts() {
	if (class_exists('PLShareBarExtended')) {
		?><script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script><script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script><?php
	}
}
add_action('wp_head', 'sbe_scripts');