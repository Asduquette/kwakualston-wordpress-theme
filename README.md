# [K5 Custom Wordpress Theme](http://kwakualston.com/home)



## Features
* Designed by [de.MO](http://demo.de.com/)in 2011 and 2015
* Coding by[Andrea Duquette](http://duquette.design)
* Wordpress 4.1.1
* Sass for stylesheets
* [BrowserSync](http://www.browsersync.io/)for synchronized browser testing
